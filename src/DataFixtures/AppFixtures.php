<?php

namespace App\DataFixtures;

use App\Entity\Vehicule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $vehicules = new Vehicule();
        $vehicules->setNom('Buggati');
        $vehicules->setCouleur('rouge');
        $vehicules->setPrix(10);
        $manager->persist($vehicules);
        $manager->flush();
    }
}
