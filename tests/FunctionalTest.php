<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testVehiculedisplay()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/vehicule/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Vehicule');
    }



}
