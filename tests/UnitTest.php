<?php

namespace App\Tests;

use App\Entity\Vehicule;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testDemo(): void
    {
        $vehicule = new Vehicule();
        $vehicule->setNom('demo');
        $this->assertTrue($vehicule->getNom() === 'demo');
    }
}
